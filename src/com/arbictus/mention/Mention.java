package com.arbictus.mention;

import com.arbictus.mention.events.NameMention;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Mention extends JavaPlugin {

    public void onEnable() {
        Bukkit.getServer().getPluginManager().registerEvents(new NameMention(this), this);
    }

    public void onDisable() {

    }

}
