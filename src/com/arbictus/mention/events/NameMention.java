package com.arbictus.mention.events;

import com.arbictus.mention.Mention;
import org.bukkit.ChatColor;
import org.bukkit.Instrument;
import org.bukkit.Note;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Created by Caidan Williams on 6/26/2016.
 */
public class NameMention implements Listener {

    private Mention mention;
    public Note notification;
    public Instrument notificationInstrument;

    public NameMention(Mention mention) {
        this.mention = mention;
        notification = new Note(1);
        notificationInstrument = Instrument.PIANO;
    }

    @EventHandler
    public void onPluginRecievedMessage(AsyncPlayerChatEvent event) {
        String message = event.getMessage();

        for (Player player: mention.getServer().getOnlinePlayers()) {
            // Check to see if player was mentioned
            if (message.contains(player.getName())) {
                player.playNote(player.getLocation(), notificationInstrument, notification);
                player.sendMessage(ChatColor.RED + event.getPlayer().getName() + " mentioned you!");
            }
        }
    }

}
